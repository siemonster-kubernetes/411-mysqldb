-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: mysqldb    Database: fouroneone
-- ------------------------------------------------------
-- Server version	5.6.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alert_logs`
--

DROP TABLE IF EXISTS `alert_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alert_logs` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` bigint(20) unsigned NOT NULL,
  `alert_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `action` tinyint(4) unsigned NOT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `a` bigint(20) unsigned NOT NULL,
  `b` bigint(20) unsigned NOT NULL,
  `archived` bigint(20) unsigned NOT NULL,
  `create_date` bigint(20) unsigned NOT NULL,
  `update_date` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `alert_idx` (`alert_id`),
  KEY `user_idx` (`user_id`),
  KEY `action_idx` (`action`),
  KEY `a_idx` (`a`),
  KEY `b_idx` (`b`),
  KEY `archived_idx` (`archived`),
  KEY `create_date_idx` (`create_date`),
  KEY `update_date_idx` (`update_date`),
  KEY `site_id_idx` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alert_logs`
--

LOCK TABLES `alert_logs` WRITE;
/*!40000 ALTER TABLE `alert_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `alert_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alerts`
--

DROP TABLE IF EXISTS `alerts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alerts` (
  `alert_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `alert_date` bigint(20) unsigned NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_hash` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_id` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assignee_type` tinyint(4) NOT NULL,
  `assignee` bigint(20) unsigned NOT NULL,
  `search_id` bigint(20) unsigned NOT NULL,
  `escalated` tinyint(1) NOT NULL,
  `state` int(11) NOT NULL,
  `archived` bigint(20) unsigned NOT NULL,
  `create_date` bigint(20) unsigned NOT NULL,
  `update_date` bigint(20) unsigned NOT NULL,
  `site_id` bigint(20) unsigned NOT NULL,
  `renderer_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `resolution` tinyint(1) NOT NULL,
  PRIMARY KEY (`alert_id`),
  KEY `alert_date_idx` (`alert_date`),
  KEY `search_id_idx` (`search_id`),
  KEY `assignee_idx` (`assignee`),
  KEY `assignee_type_idx` (`assignee_type`),
  KEY `escalated_idx` (`escalated`),
  KEY `state_idx` (`state`),
  KEY `content_hash_idx` (`content_hash`),
  KEY `archived_idx` (`archived`),
  KEY `create_date_idx` (`create_date`),
  KEY `update_date_idx` (`update_date`),
  KEY `site_id_idx` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alerts`
--

LOCK TABLES `alerts` WRITE;
/*!40000 ALTER TABLE `alerts` DISABLE KEYS */;
/*!40000 ALTER TABLE `alerts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `site_id` bigint(20) unsigned NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`site_id`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES (1,'cookie_secret','mVRw+2COvFlYuBwfk6LBWJorfuhay6I4'),(1,'cron_enabled','1'),(1,'default_email','admin@siemonster.io'),(1,'error_email_enabled','1'),(1,'error_email_throttle','30'),(1,'from_email','alert@siemonster.io'),(1,'from_error_email','error@siemonster.io'),(1,'last_cron_date','0'),(1,'last_rollup_date','0'),(1,'summary_enabled','1'),(1,'timezone','UTC'),(1,'worker_enabled','1');
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_targets`
--

DROP TABLE IF EXISTS `group_targets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_targets` (
  `group_target_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) unsigned NOT NULL,
  `type` tinyint(4) NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `data` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `archived` bigint(20) unsigned NOT NULL,
  `create_date` bigint(20) unsigned NOT NULL,
  `update_date` bigint(20) unsigned NOT NULL,
  `site_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`group_target_id`),
  KEY `group_id_idx` (`group_id`),
  KEY `archived_idx` (`archived`),
  KEY `create_date_idx` (`create_date`),
  KEY `update_date_idx` (`update_date`),
  KEY `site_id_idx` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_targets`
--

LOCK TABLES `group_targets` WRITE;
/*!40000 ALTER TABLE `group_targets` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_targets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `group_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NOT NULL,
  `state` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `archived` bigint(20) unsigned NOT NULL,
  `create_date` bigint(20) unsigned NOT NULL,
  `update_date` bigint(20) unsigned NOT NULL,
  `site_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `type_idx` (`type`),
  KEY `archived_idx` (`archived`),
  KEY `create_date_idx` (`create_date`),
  KEY `update_date_idx` (`update_date`),
  KEY `site_id_idx` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `job_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` bigint(20) unsigned NOT NULL,
  `type` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target_id` bigint(20) unsigned NOT NULL,
  `state` tinyint(4) unsigned NOT NULL,
  `completion` tinyint(4) unsigned NOT NULL,
  `tries` int(20) unsigned NOT NULL,
  `target_date` bigint(20) unsigned NOT NULL,
  `last_execution_date` bigint(20) unsigned NOT NULL,
  `archived` bigint(20) unsigned NOT NULL,
  `create_date` bigint(20) unsigned NOT NULL,
  `update_date` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`job_id`),
  KEY `type_idx` (`type`),
  KEY `target_id_idx` (`target_id`),
  KEY `state_idx` (`state`),
  KEY `tries_idx` (`tries`),
  KEY `last_execution_date_idx` (`last_execution_date`),
  KEY `site_id_idx` (`site_id`),
  KEY `archived_idx` (`archived`),
  KEY `create_date_idx` (`create_date`),
  KEY `update_date_idx` (`update_date`),
  KEY `target_date_idx` (`target_date`),
  KEY `type_target_id_site_id_archived_idx` (`type`,`target_id`,`site_id`,`archived`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
INSERT INTO `jobs` VALUES (1,1,'rollup',0,1,100,1,1522456731,1522456741,0,1522456741,1522456741),(2,1,'rollup',1,1,100,1,1522456731,1522456741,0,1522456741,1522456741),(3,1,'autoclose',0,1,100,1,1522456731,1522456741,0,1522456741,1522456741),(4,1,'cleanup',0,1,100,1,1522456731,1522456741,0,1522456741,1522456741);
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lists`
--

DROP TABLE IF EXISTS `lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lists` (
  `list_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NOT NULL,
  `archived` bigint(20) unsigned NOT NULL,
  `create_date` bigint(20) unsigned NOT NULL,
  `update_date` bigint(20) unsigned NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`list_id`),
  KEY `type_idx` (`type`),
  KEY `archived_idx` (`archived`),
  KEY `create_date_idx` (`create_date`),
  KEY `update_date_idx` (`update_date`),
  KEY `name_idx` (`name`),
  KEY `site_id_idx` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lists`
--

LOCK TABLES `lists` WRITE;
/*!40000 ALTER TABLE `lists` DISABLE KEYS */;
/*!40000 ALTER TABLE `lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta`
--

DROP TABLE IF EXISTS `meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta` (
  `site_id` bigint(20) unsigned NOT NULL,
  `key` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`site_id`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta`
--

LOCK TABLES `meta` WRITE;
/*!40000 ALTER TABLE `meta` DISABLE KEYS */;
INSERT INTO `meta` VALUES (1,'last_cron_date','1522456912'),(1,'search_graphite[graphite]','0'),(1,'search_ping','0');
/*!40000 ALTER TABLE `meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_targets`
--

DROP TABLE IF EXISTS `report_targets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_targets` (
  `report_target_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `report_id` bigint(20) unsigned NOT NULL,
  `search_id` bigint(20) unsigned NOT NULL,
  `position` int(11) NOT NULL,
  `archived` bigint(20) unsigned NOT NULL,
  `create_date` bigint(20) unsigned NOT NULL,
  `update_date` bigint(20) unsigned NOT NULL,
  `site_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`report_target_id`),
  KEY `report_id_idx` (`report_id`),
  KEY `archived_idx` (`archived`),
  KEY `create_date_idx` (`create_date`),
  KEY `update_date_idx` (`update_date`),
  KEY `site_id_idx` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_targets`
--

LOCK TABLES `report_targets` WRITE;
/*!40000 ALTER TABLE `report_targets` DISABLE KEYS */;
/*!40000 ALTER TABLE `report_targets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reports`
--

DROP TABLE IF EXISTS `reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reports` (
  `report_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `range` int(11) NOT NULL,
  `archived` bigint(20) unsigned NOT NULL,
  `create_date` bigint(20) unsigned NOT NULL,
  `update_date` bigint(20) unsigned NOT NULL,
  `frequency` bigint(20) unsigned NOT NULL,
  `assignee_type` tinyint(4) NOT NULL,
  `assignee` bigint(20) unsigned NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `start_date` bigint(20) unsigned NOT NULL,
  `site_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`report_id`),
  KEY `type_idx` (`type`),
  KEY `archived_idx` (`archived`),
  KEY `create_date_idx` (`create_date`),
  KEY `update_date_idx` (`update_date`),
  KEY `site_id_idx` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reports`
--

LOCK TABLES `reports` WRITE;
/*!40000 ALTER TABLE `reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_filters`
--

DROP TABLE IF EXISTS `search_filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_filters` (
  `filter_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `search_id` bigint(20) unsigned NOT NULL,
  `type` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `archived` bigint(20) unsigned NOT NULL,
  `create_date` bigint(20) unsigned NOT NULL,
  `update_date` bigint(20) unsigned NOT NULL,
  `lifetime` bigint(20) unsigned NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`filter_id`),
  KEY `search_id_idx` (`search_id`),
  KEY `archived_idx` (`archived`),
  KEY `create_date_idx` (`create_date`),
  KEY `update_date_idx` (`update_date`),
  KEY `site_id_idx` (`site_id`),
  KEY `site_id_archived_lifetime_idx` (`site_id`,`archived`,`lifetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_filters`
--

LOCK TABLES `search_filters` WRITE;
/*!40000 ALTER TABLE `search_filters` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_filters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_logs`
--

DROP TABLE IF EXISTS `search_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_logs` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `search_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `archived` bigint(20) unsigned NOT NULL,
  `create_date` bigint(20) unsigned NOT NULL,
  `update_date` bigint(20) unsigned NOT NULL,
  `site_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `search_id_idx` (`search_id`),
  KEY `archived_idx` (`archived`),
  KEY `create_date_idx` (`create_date`),
  KEY `update_date_idx` (`update_date`),
  KEY `site_id_idx` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_logs`
--

LOCK TABLES `search_logs` WRITE;
/*!40000 ALTER TABLE `search_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `search_targets`
--

DROP TABLE IF EXISTS `search_targets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_targets` (
  `target_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `search_id` bigint(20) unsigned NOT NULL,
  `type` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `archived` bigint(20) unsigned NOT NULL,
  `create_date` bigint(20) unsigned NOT NULL,
  `update_date` bigint(20) unsigned NOT NULL,
  `lifetime` bigint(20) unsigned NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`target_id`),
  KEY `search_id_idx` (`search_id`),
  KEY `archived_idx` (`archived`),
  KEY `create_date_idx` (`create_date`),
  KEY `update_date_idx` (`update_date`),
  KEY `site_id_idx` (`site_id`),
  KEY `site_id_archived_lifetime_idx` (`site_id`,`archived`,`lifetime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `search_targets`
--

LOCK TABLES `search_targets` WRITE;
/*!40000 ALTER TABLE `search_targets` DISABLE KEYS */;
/*!40000 ALTER TABLE `search_targets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `searches`
--

DROP TABLE IF EXISTS `searches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `searches` (
  `search_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `query_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tags` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` tinyint(1) NOT NULL,
  `frequency` int(11) NOT NULL,
  `range` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `assignee_type` tinyint(4) NOT NULL,
  `assignee` bigint(20) unsigned NOT NULL,
  `owner` bigint(20) unsigned NOT NULL,
  `last_execution_date` bigint(20) unsigned NOT NULL,
  `last_success_date` bigint(20) unsigned NOT NULL,
  `archived` bigint(20) unsigned NOT NULL,
  `schedule_type` tinyint(4) NOT NULL,
  `create_date` bigint(20) unsigned NOT NULL,
  `update_date` bigint(20) unsigned NOT NULL,
  `cron_expression` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_failure_date` bigint(20) unsigned NOT NULL,
  `last_error_email_date` bigint(20) unsigned NOT NULL,
  `renderer_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_status` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `flap_rate` double NOT NULL,
  `notif_type` tinyint(1) NOT NULL,
  `notif_format` tinyint(1) NOT NULL,
  `notif_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoclose_threshold` int(11) DEFAULT NULL,
  `state_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `type_idx` (`type`),
  KEY `category_idx` (`category`),
  KEY `tags_idx` (`tags`),
  KEY `priority_idx` (`priority`),
  KEY `enabled_idx` (`enabled`),
  KEY `assignee_type_idx` (`assignee_type`),
  KEY `assignee_idx` (`assignee`),
  KEY `owner_idx` (`owner`),
  KEY `archived_idx` (`archived`),
  KEY `create_date_idx` (`create_date`),
  KEY `update_date_idx` (`update_date`),
  KEY `site_id_idx` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `searches`
--

LOCK TABLES `searches` WRITE;
/*!40000 ALTER TABLE `searches` DISABLE KEYS */;
/*!40000 ALTER TABLE `searches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites`
--

DROP TABLE IF EXISTS `sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites` (
  `site_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `host` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secure` tinyint(1) NOT NULL,
  `archived` tinyint(1) NOT NULL,
  `create_date` bigint(20) unsigned NOT NULL,
  `update_date` bigint(20) unsigned NOT NULL,
  KEY `site_id_idx` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites`
--

LOCK TABLES `sites` WRITE;
/*!40000 ALTER TABLE `sites` DISABLE KEYS */;
INSERT INTO `sites` VALUES (1,'SIEMonster Alerts','411.vmware.portal.siemonster.com',0,0,1522456656,1522456656);
/*!40000 ALTER TABLE `sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slogs`
--

DROP TABLE IF EXISTS `slogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slogs` (
  `slog_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` bigint(20) unsigned NOT NULL,
  `type` tinyint(4) unsigned NOT NULL,
  `action` tinyint(4) unsigned NOT NULL,
  `target` bigint(20) unsigned NOT NULL,
  `actor` bigint(20) unsigned NOT NULL,
  `a` bigint(20) unsigned NOT NULL,
  `b` bigint(20) unsigned NOT NULL,
  `archived` bigint(20) unsigned NOT NULL,
  `create_date` bigint(20) unsigned NOT NULL,
  `update_date` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`slog_id`),
  KEY `type_idx` (`type`),
  KEY `action_idx` (`action`),
  KEY `target_idx` (`target`),
  KEY `actor_idx` (`actor`),
  KEY `a_idx` (`a`),
  KEY `b_idx` (`b`),
  KEY `archived_idx` (`archived`),
  KEY `create_date_idx` (`create_date`),
  KEY `update_date_idx` (`update_date`),
  KEY `site_id_idx` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slogs`
--

LOCK TABLES `slogs` WRITE;
/*!40000 ALTER TABLE `slogs` DISABLE KEYS */;
/*!40000 ALTER TABLE `slogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` bigint(20) unsigned NOT NULL,
  `name` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `real_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timezone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `settings` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_key` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `archived` bigint(20) unsigned NOT NULL,
  `create_date` bigint(20) unsigned NOT NULL,
  `update_date` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `site_id_name_idx` (`site_id`,`name`),
  UNIQUE KEY `site_id_api_key_idx` (`site_id`,`api_key`),
  KEY `site_id_idx` (`site_id`),
  KEY `archived_idx` (`archived`),
  KEY `create_date_idx` (`create_date`),
  KEY `update_date_idx` (`update_date`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'admin','Admin','$2y$10$V4zcSgXca5yYPaRtXtoKAuQXmdRbQcH/42JnQjIOxUIqTPFzu39C6','admin@example.com','',1,'{}','DTlsgKhmdOt01ObWy8CG5DEulkhWrLSrKp24B7Y3',0,1522456800,1522456800);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-31  0:42:18
